##  Q1. What is a pseudo-class?
 It is a CSS technique that sets the style when an element changes its state. E.g. style changes when mouse hover, different styles for visited or unvisited links, etc.

There are four different types of **pseudo-classes**:

**Links**: These pseudo-classes are used to style the link both in its normal state by default and when it has already been visited
**Dynamic**: Dynamic pseudo-classes can be applied to any element to define how they are shown when the cursor is located on them, or by clicking on them or when they are selected
**Structural**: Structural pseudo-classes allow to style elements based on an exact numerical position of the element
Others: Some elements can be styled differently based on the language or what type of label they are not.

## What is DOM (Document Object Model)? [CSS]
 The Document Object Model (DOM) is a cross-platform programming interface that represents HTML and XML documents as nodes and objects. In simple terms, it defines the logical structure of documents and the way the documents are accessed and manipulated. DOM enables programmers to create, modify, and delete the document structure, style, and content. 

When a document is displayed on a browser, the content of the document must be combined with its style information. The browser converts HTML and CSS into the DOM. The DOM combines the document’s content with its style.

## What is Cross-site Scripting (XSS)?
👉  Cross-site scripting (XSS) is a security exploit that enables an attacker to introduce malicious client-side code into a website. When the untrusted links are unintentionally clicked by the victims, the cookies information is passed to the attackers so that they can bypass access controls.

## Explain the functional and non-functional requirements?
 Functional requirements define the specific functionality of the system, It describes what the system does or must not do. 

Non-functional requirements, on the other hand, define how the system should do it. It specifies a system’s type, in terms of accessibility, reliability, capacity, usability, maintainability, and security. Non-functional requirements describe system behavior, features, and general characteristics that affect the user experience.

Non-functional requirements do not affect the basic functionality of the system. The system will continue to perform its basic purpose, even if the non-functional requirements are not met.

## What is Type Coercion in JavaScript?
 Type coercion refers to the conversion of a value from one type to another (e.g Number to String, String to Number, or Boolean to Number) with similar content. In case the behavior of the implicit conversion is not sure, then the constructors of a data type can be used to convert any value to that datatype.

## What is Scope in JavaScript? Name the different types of Scopes.
 The scope defines the accessibility of the functions and variables in an application. There are two types of scopes: local and global.
 