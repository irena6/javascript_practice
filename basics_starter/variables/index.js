let name = 'irene';
console.log(name);


/* let name;
console.log(name); // value is undefined in javascript */

// Cannot be a reserved keywords
// Should be meaningful
//Cannot start with a number(1name)
// Cannot contain a space or hyphen (-)
// Are case sensitive

// variables are used to store data temporarily, in a computers memory,
// variable is like a box, what we put inside that box is the value that we assign to 
// the names we put inside the box are the name of the variables